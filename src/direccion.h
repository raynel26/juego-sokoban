//
// Created by Angely on 11/12/2020.
//
#include <iostream>
#include "Tipos.h"

#ifndef TP4_SOKOBAN_DIRECCION_H
#define TP4_SOKOBAN_DIRECCION_H



class direccion{
public:
    direccion(PuntoCardinal p);
    Coordenada operator+(Coordenada c);
private:
    PuntoCardinal _direccion;
};
#endif //TP4_SOKOBAN_DIRECCION_H
