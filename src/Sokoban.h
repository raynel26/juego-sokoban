#include "Tipos.h"
#include "Nivel.h"
#include <stack>
#include "direccion.h"
#ifndef TP4_SOKOBAN_SOKOBAN_H
#define TP4_SOKOBAN_SOKOBAN_H

class Sokoban{

public:
    Sokoban(const Nivel &n);

    bool Mover(direccion d);

    void ArrojarBomba();

    void DeshacerUltMovimiento();

    const Mapa& Map() const;

    int Bombas() const;

    Coordenada PosicionPersona() const;

    int HayCaja(Coordenada c) const;

    bool laExplotoUnaBomba(Coordenada c) const;

    bool puedoMover(direccion d) const;

    bool noHayParedNiCaja(Coordenada c) const;

    bool haypared(Coordenada c) const;

    bool haycaja(Coordenada c) const;
private:

    struct acciones{
        bool moviJugador;
        bool moviCaja;
        bool tireBomba;
    };
    struct infoCajas{
        infoCajas(int indice, Coordenada p, int e): indice(indice), PosicionCaja(p), SumaDeposito(e){}
        int indice;
        Coordenada PosicionCaja;
        int SumaDeposito;
    };

    Nivel _partida;
    list<Coordenada> _BombasArrojadas;
    stack<acciones> _acciones;
    stack<infoCajas> _historialDeMovimientoCajas;
    stack<Coordenada> _historialDePosiciones;
    int _depositosllenos;  // tengo que inicializarlo con la contidad de depositos llenos que me trae el nivel
};


#endif //TP4_SOKOBAN_SOKOBAN_H
