#include "Sokoban.h"

Sokoban::Sokoban(const Nivel &n) :_partida(n), _depositosllenos(n.DepositosLLenosAliniciar()), _acciones(), _historialDePosiciones(), _historialDeMovimientoCajas(), _BombasArrojadas() {
}

bool Sokoban::Mover(direccion d){
    bool res = false;
    if(puedoMover(d)){
       acciones a;
       a.moviCaja = false;
       a.moviJugador = true;
       a.tireBomba = false;

       Coordenada posicionAntesDeMover = PosicionPersona();
       Coordenada posicionDespuesDeMover = d + posicionAntesDeMover;
       _historialDePosiciones.push(posicionAntesDeMover);
       _partida.moverPersona(posicionDespuesDeMover);

       int i = HayCaja(posicionDespuesDeMover);
       if (i != -1){
           a.moviCaja = true;
            _partida.moverCaja(i, d + posicionDespuesDeMover);
            bool hayDepositoEnPosicionAnterior = Map().hayDeposito(posicionDespuesDeMover);
            bool hayDepositoEnPosisionPosterior = Map().hayDeposito(d + posicionDespuesDeMover);

            if(hayDepositoEnPosicionAnterior && !hayDepositoEnPosisionPosterior){
                infoCajas info = infoCajas(i, posicionDespuesDeMover, -1);
                _historialDeMovimientoCajas.push(info);
                _depositosllenos--;
            }else{
                if (!hayDepositoEnPosicionAnterior && hayDepositoEnPosisionPosterior){
                    infoCajas info = infoCajas(i, posicionDespuesDeMover, 1);
                    _historialDeMovimientoCajas.push(info);
                    _depositosllenos++;
                }else{
                    infoCajas info = infoCajas(i, posicionDespuesDeMover, 0);
                    _historialDeMovimientoCajas.push(info);
                }
            }
            res = _depositosllenos == _partida.Cajas().size();
       }
        _acciones.push(a);
    }
    return res;
}

void Sokoban::DeshacerUltMovimiento(){
    if (_acciones.top().moviJugador){       //si movi al jugador
        _partida.moverPersona(_historialDePosiciones.top());
        _historialDePosiciones.pop();
        if (_acciones.top().moviCaja){      //si movi caja
            if (_historialDeMovimientoCajas.top().SumaDeposito == 1){
                _depositosllenos--;
            }else{
                if (_historialDeMovimientoCajas.top().SumaDeposito == -1){
                    _depositosllenos++;
                }
            }
            _partida.moverCaja(_historialDeMovimientoCajas.top().indice,_historialDeMovimientoCajas.top().PosicionCaja);
            _historialDeMovimientoCajas.pop();
        }
    }
    if (_acciones.top().tireBomba){
        _BombasArrojadas.pop_front(); //eliminar primer elemento de la lista
        _partida.modificarCantDeBombas(true);
    }
    _acciones.pop();
}

void Sokoban::ArrojarBomba(){
    if(Bombas() > 0){
        acciones a;
        a.moviCaja = false;
        a.moviJugador = false;
        a.tireBomba = true;

        _acciones.push(a);

        _BombasArrojadas.push_front(PosicionPersona()); // insertar en primera posicion;
        _partida.modificarCantDeBombas(false);


    }
}

int Sokoban::HayCaja(Coordenada c) const{
    return _partida.haycaja(c);
}

bool Sokoban::puedoMover(direccion d) const{
    Coordenada posicionAmoverse = d + PosicionPersona();
    bool a = noHayParedNiCaja(posicionAmoverse) || (haycaja(posicionAmoverse) && noHayParedNiCaja(d + posicionAmoverse));
    return a;
}

bool Sokoban::noHayParedNiCaja(Coordenada c) const{
    return !haypared(c) && !haycaja(c);
}

//-----------------------------------------------------------------------------------//
bool Sokoban::laExplotoUnaBomba(Coordenada c) const{
   bool res = false;
   for (Coordenada e:_BombasArrojadas) {
       if (e.first == c.first || e.second == c.second){
           res |= true;
       }
   }
   return res;
}

Coordenada Sokoban::PosicionPersona() const{
    return _partida.PosicionPersona();
}

const Mapa& Sokoban:: Map() const{
    return _partida.Map();
}

int Sokoban::Bombas() const{
    return _partida.Bombas();
}

bool Sokoban::haycaja(Coordenada c) const{
    return _partida.hayCaja(c);
}

bool Sokoban::haypared(Coordenada c) const{
    return Map().hayPared(c) && !laExplotoUnaBomba(c);
}