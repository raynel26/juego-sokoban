#include "direccion.h"

direccion::direccion(PuntoCardinal p) : _direccion(p){}

Coordenada direccion::operator+(Coordenada c) {
    Coordenada d;
    if (_direccion == Norte){
        d.first = c.first;
        d.second = c.second + 1;
    }else{
        if (_direccion == Sur){
            d.first = c.first;
            d.second = c.second - 1;
        }else{
            if (_direccion == Este){
                d.first = c.first + 1;
                d.second = c.second;
            }else{
                d.first = c.first - 1;
                d.second = c.second;
            }
        }
    }
    return d;
}