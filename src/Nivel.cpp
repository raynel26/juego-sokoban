#include "Nivel.h"

Nivel::Nivel(Mapa m, Coordenada posicionInicial, vector<Coordenada> cajas, int cantBombas)
: _mapa((m)), _posicionActual(posicionInicial), _cajas(cajas), _cantBombas(cantBombas){}

int Nivel::haycaja(Coordenada caja) const{
    int i = 0;
    while (i < _cajas.size() && !(caja == _cajas[i])){
        i++;
    }
    if(i == _cajas.size()){
        return -1;
    }else{
        return i;
    }
}

const Mapa& Nivel::Map() const{
    return _mapa;
}

Nat Nivel::Bombas() const{
    return _cantBombas;
}

Coordenada Nivel::PosicionPersona() const{
    return _posicionActual;
}

const vector<Coordenada> & Nivel::Cajas() {
    return _cajas;
}

//------ OPERACIONES DE MODIFICACION-------//
void Nivel::moverPersona(Coordenada c) {
    _posicionActual = c;
}

void Nivel::modificarCantDeBombas(bool inc) {
    if (inc == true){
        _cantBombas++;
    }else{
        _cantBombas--;
    }
}
void Nivel::moverCaja(int i, Coordenada c) {
    _cajas[i] = c;
}

Nat Nivel::DepositosLLenosAliniciar() const{
    int count = 0;
    int i = 0;
    int longiturCajas = _cajas.size();
    while (i < longiturCajas){
        if(Map().hayDeposito(_cajas[i])){
            count++;
        }
        i++;
    }
    return count;
}

Nivel::Nivel(const aed2_Nivel &ns) {
 _posicionActual = ns.posicionInicial;
 _cantBombas = ns.cantidadBombas;
 for(Coordenada c: ns.cajas){
     _cajas.push_back(c);
 }
 _mapa = (Mapa(ns.paredes, ns.depositos));
}

bool Nivel::hayCaja(Coordenada c) const{
    int i = 0;
    while (i < _cajas.size() && c != _cajas[i]){
        i++;
    }
    return i < _cajas.size();
}