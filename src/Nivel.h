#ifndef TP4_SOKOBAN_NIVEL_H
#define TP4_SOKOBAN_NIVEL_H

#include "Tipos.h"
#include <vector>
#include "Mapa.h"

class Nivel {
public:
    Nivel(const aed2_Nivel &ns);
    Nivel(Mapa m, Coordenada posicionInicial, vector<Coordenada> cajas, int cantBombas);

    const Mapa& Map() const;

    Coordenada PosicionPersona() const;

    const vector<Coordenada> &Cajas();

    Nat Bombas() const;

    void moverPersona(Coordenada c);

    void modificarCantDeBombas(bool inc);

    void moverCaja(int i, Coordenada c);

    Nat DepositosLLenosAliniciar() const;

    int haycaja(Coordenada caja) const;

    bool hayCaja(Coordenada c) const;

private:
    Mapa _mapa;
    Coordenada _posicionActual;
    vector<Coordenada> _cajas;
    Nat _cantBombas;
};
#endif //TP4_SOKOBAN_NIVEL_H
