#ifndef TP4_SOKOBAN_JUEGO_H
#define TP4_SOKOBAN_JUEGO_H

#include "Tipos.h"
#include <vector>
#include "Sokoban.h"

class Juego{

public:
    Juego(vector<aed2_Nivel> j);

    void Mover(direccion p);

    void ArrojarBomba();

    void DeshacerUltMovimiento();

private:
    Sokoban _actual;
    vector<aed2_Nivel> _partidas;
    int _i;
};

#endif //TP4_SOKOBAN_JUEGO_H
