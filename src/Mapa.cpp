#include "Mapa.h"

Mapa::Mapa(): _Depositos(), _Paredes(){}

bool Mapa::hayPared(Coordenada c) const{
    return BusquedaBinaria(_Paredes, c);
}

bool Mapa::hayDeposito(Coordenada c) const{
    return BusquedaBinaria(_Depositos, c);
}

void Mapa::AgDeposito(Coordenada c) {
    _Depositos.push_back(c);
    insertar(_Depositos);
}

void Mapa::AgPared(Coordenada c) {
    _Paredes.push_back(c);
    insertar(_Paredes);
}

const vector<Coordenada>& Mapa::Paredes() {
    return _Paredes;
}

const vector<Coordenada>& Mapa::Depositos() {
    return _Depositos;
}

void Mapa::insertar(vector<Coordenada>& x) {
    int i = x.size() - 1;
    for (int j = i; j > 0 && coordenadaMayor(x[j], x[j - 1]); j--) {
        Coordenada c = x[j];
        x[j] = x[j - 1];
        x[j - 1] = c;
    }
}

bool Mapa::BusquedaBinaria(const vector<Coordenada> &x, Coordenada c) const{
    int longitud = x.size();
    if (longitud == 0){
        return false;
    } else {
        if (longitud == 1) {
            return c == x[0];
        } else {
            if (coordenadaMayor(c, x[0])) {
                return false;
            } else {
                if (coordenadaMayor(x[longitud - 1], c) || c == x[longitud - 1]){
                    return c == x[longitud - 1];
                } else {
                    int i = 0;
                    int j = longitud - 1;
                    while (j > i + 1) {
                        int k = (i + j) / 2;
                        if (coordenadaMayor(c, x[k])) {
                            j = k;
                        } else {
                            i = k;
                        }
                    }
                    return c == x[i];

                }
            }
        }
    }
}

Mapa::Mapa(set<Coordenada> paredes, set<Coordenada> depositos) {
    for(Coordenada c: paredes){
        AgPared(c);
    }
    for (Coordenada c: depositos){
        AgDeposito(c);
    }
}

bool Mapa::coordenadaMayor(Coordenada c, Coordenada d) const{
    bool res = false;
    if (c.first < d.first){
        res = true;
    }else{
        if (c.first == d.first){
            res = c.second < d.second;
        }
    }
    return res;
}