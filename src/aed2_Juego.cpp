#include "aed2_Juego.h"


aed2_Juego::aed2_Juego(const vector<aed2_Nivel> &ns) : _niveles(ns), _actual(Sokoban(Nivel(ns[0]))), _i(0){
    Nivel n = Nivel(ns[0]);
}

bool aed2_Juego::hayPared(Coordenada c) const{
    return _actual.haypared(c);
}

bool aed2_Juego::hayDeposito(Coordenada c)  const{
    return _actual.Map().hayDeposito(c);
}

bool aed2_Juego::hayCaja(Coordenada c)  const{
    return _actual.haycaja(c);
}

Coordenada aed2_Juego::posicionActual()  const{
    return _actual.PosicionPersona();
}

Nat aed2_Juego::cantidadBombas()  const{
    return _actual.Bombas();
}

bool aed2_Juego::sePuedeMover(PuntoCardinal pc)  const{
    return _actual.puedoMover(direccion(pc));
}

// IMPORTANTE: Debe devolver:
// - true si al mover se completa el nivel actual
// - false en caso contrario.
bool aed2_Juego::mover(PuntoCardinal pc) {
    bool gane = _actual.Mover(direccion(pc));
    if (gane && _i + 1 < _niveles.size()){
        Nivel n = Nivel(_niveles[_i + 1]);
        _actual = Sokoban(n);
        _i++;
    }
    return gane;
}

void aed2_Juego::tirarBomba() {
    _actual.ArrojarBomba();
}

void aed2_Juego::deshacer() {
    _actual.DeshacerUltMovimiento();
}

