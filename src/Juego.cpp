#include "Juego.h"


Juego::Juego(vector<aed2_Nivel> j) : _actual(Sokoban(Nivel(j[0]))), _partidas(j), _i(0){}

void Juego::Mover(direccion p){
    bool gane = _actual.Mover(p);
    if (gane && _i + 1 < _partidas.size()){
        _actual = Sokoban(Nivel(_partidas[_i + 1]));
    }
}

void Juego::ArrojarBomba() {
    _actual.ArrojarBomba();
}

void Juego::DeshacerUltMovimiento() {
    _actual.DeshacerUltMovimiento();
}

