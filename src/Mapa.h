#include "Tipos.h"
#include <vector>
#include <list>

#ifndef TP4_SOKOBAN_MAPA_H
#define TP4_SOKOBAN_MAPA_H

class Mapa{

public:
    Mapa(set<Coordenada> paredes, set<Coordenada> depositos);
    Mapa();

    void AgPared(Coordenada c);

    void AgDeposito(Coordenada c);

    const vector<Coordenada>& Paredes();

    const vector<Coordenada>& Depositos();

    bool hayDeposito(Coordenada c) const;

    bool hayPared(Coordenada c) const;

    bool coordenadaMayor(Coordenada c, Coordenada d) const;

private:
    bool BusquedaBinaria(const vector<Coordenada> &x, Coordenada c) const;
    void insertar(vector<Coordenada>&);
    vector<Coordenada> _Depositos;
    vector<Coordenada> _Paredes;
};

#endif //TP4_SOKOBAN_MAPA_H
